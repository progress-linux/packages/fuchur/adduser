# Vietnamese runtime translation for AddUser.
# Copyright © 2010 Free Software Foundation, Inc.
# This file is distributed under the same license as the adduser package.
# Clytie Siddall <clytie@riverland.net.au>, 2010.
# Trần Ngọc Quân <vnwildman@gmail.com>, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: adduser 3.114\n"
"Report-Msgid-Bugs-To: adduser@packages.debian.org\n"
"POT-Creation-Date: 2016-06-17 18:51+0200\n"
"PO-Revision-Date: 2016-06-17 19:08+0200\n"
"Last-Translator: Trần Ngọc Quân <vnwildman@gmail.com>\n"
"Language-Team: Vietnamese <debian-l10n-vietnamese@lists.debian.org>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Gtranslator 2.91.7\n"

#. everyone can issue "--help" and "--version", but only root can go on
#: ../adduser:150
msgid "Only root may add a user or group to the system.\n"
msgstr ""
"Chỉ siêu quản trị có quyền thêm vào hệ thống một người dùng hay nhóm.\n"

#: ../adduser:176 ../deluser:137
msgid "Only one or two names allowed.\n"
msgstr "Chỉ cho phép một hay hai tên thôi.\n"

#. must be addusertogroup
#: ../adduser:181
msgid "Specify only one name in this mode.\n"
msgstr "Trong chế độ này thì chỉ đưa ra một tên.\n"

#: ../adduser:197
msgid "The --group, --ingroup, and --gid options are mutually exclusive.\n"
msgstr "Ba tùy chọn “--group”, “--ingroup” và “--gid options” xung đột nhau.\n"

#: ../adduser:202
msgid "The home dir must be an absolute path.\n"
msgstr "Thư mục chính phải là một đường dẫn tuyệt đối.\n"

#: ../adduser:206
#, perl-format
msgid "Warning: The home dir %s you specified already exists.\n"
msgstr "Cảnh báo: bạn đã ghi rõ một thư mục chính %s đã có.\n"

#: ../adduser:208
#, perl-format
msgid "Warning: The home dir %s you specified can't be accessed: %s\n"
msgstr ""
"Cảnh báo: bạn đã ghi rõ một thư mục chính %s không cho truy cập được: %s\n"

#: ../adduser:270
#, perl-format
msgid "The group `%s' already exists as a system group. Exiting.\n"
msgstr "Nhóm “%s” đã có như là một nhóm ở mức hệ thống nên thoát.\n"

#: ../adduser:276
#, perl-format
msgid "The group `%s' already exists and is not a system group. Exiting.\n"
msgstr "Nhóm “%s” đã có và không phải là một nhóm ở mức hệ thống nên thoát.\n"

#: ../adduser:282
#, perl-format
msgid "The group `%s' already exists, but has a different GID. Exiting.\n"
msgstr "Nhóm “%s” đã có với một GID khác nên thoát.\n"

#: ../adduser:286 ../adduser:316
#, perl-format
msgid "The GID `%s' is already in use.\n"
msgstr "GID “%s” đang được dùng.\n"

#: ../adduser:294
#, perl-format
msgid ""
"No GID is available in the range %d-%d (FIRST_SYS_GID - LAST_SYS_GID).\n"
msgstr ""
"Không có GID nào sẵn sàng trong phạm vi %d-%d (FIRST_SYS_GID - "
"LAST_SYS_GID).\n"

#: ../adduser:295 ../adduser:325
#, perl-format
msgid "The group `%s' was not created.\n"
msgstr "Chưa tạo nhóm “%s”.\n"

#: ../adduser:300 ../adduser:329
#, perl-format
msgid "Adding group `%s' (GID %d) ...\n"
msgstr "Đang thêm nhóm “%s” (GID %d) …\n"

#: ../adduser:305 ../adduser:334 ../adduser:359 ../deluser:370 ../deluser:407
#: ../deluser:444
msgid "Done.\n"
msgstr "Hoàn tất.\n"

#: ../adduser:314 ../adduser:792
#, perl-format
msgid "The group `%s' already exists.\n"
msgstr "Nhóm “%s” đã có.\n"

#: ../adduser:324
#, perl-format
msgid "No GID is available in the range %d-%d (FIRST_GID - LAST_GID).\n"
msgstr ""
"Không có GID nào sẵn sàng trong phạm vi %d-%d (FIRST_GID - LAST_GID).\n"

#: ../adduser:343 ../deluser:229 ../deluser:416
#, perl-format
msgid "The user `%s' does not exist.\n"
msgstr "Người dùng “%s” không tồn tại.\n"

#: ../adduser:345 ../adduser:591 ../adduser:799 ../deluser:378 ../deluser:419
#, perl-format
msgid "The group `%s' does not exist.\n"
msgstr "Nhóm “%s” không tồn tại.\n"

#: ../adduser:348 ../adduser:595
#, perl-format
msgid "The user `%s' is already a member of `%s'.\n"
msgstr "Người dùng “%s” đã thuộc về “%s”.\n"

#: ../adduser:353 ../adduser:601
#, perl-format
msgid "Adding user `%s' to group `%s' ...\n"
msgstr "Đang thêm người dung “%s” vào nhóm “%s” …\n"

#: ../adduser:373
#, perl-format
msgid "The system user `%s' already exists. Exiting.\n"
msgstr "Người dùng ở mức hệ thống “%s” đã có nên thoát.\n"

#: ../adduser:376
#, perl-format
msgid "The user `%s' already exists. Exiting.\n"
msgstr "Người dùng “%s” đã có nên thoát.\n"

#: ../adduser:380
#, perl-format
msgid "The user `%s' already exists with a different UID. Exiting.\n"
msgstr "Người dùng “%s” đã có với một UID khác nên thoát.\n"

#: ../adduser:394
#, perl-format
msgid ""
"No UID/GID pair is available in the range %d-%d (FIRST_SYS_UID - "
"LAST_SYS_UID).\n"
msgstr ""
"Không có cặp UID/GID nào sẵn sàng trong phạm vi %d-%d (FIRST_SYS_UID - "
"LAST_SYS_UID).\n"

#: ../adduser:395 ../adduser:407 ../adduser:487 ../adduser:499
#, perl-format
msgid "The user `%s' was not created.\n"
msgstr "Chưa tạo người dùng “%s”.\n"

#: ../adduser:406
#, perl-format
msgid ""
"No UID is available in the range %d-%d (FIRST_SYS_UID - LAST_SYS_UID).\n"
msgstr ""
"Không có UID nào sẵn sàng trong phạm vi %d-%d (FIRST_SYS_UID - "
"LAST_SYS_UID).\n"

#: ../adduser:411 ../adduser:417 ../adduser:503 ../adduser:509
msgid "Internal error"
msgstr "Lỗi nội bộ"

#: ../adduser:419
#, perl-format
msgid "Adding system user `%s' (UID %d) ...\n"
msgstr "Đang thêm người dùng ở mức hệ thống “%s” (UID %d) …\n"

#: ../adduser:424
#, perl-format
msgid "Adding new group `%s' (GID %d) ...\n"
msgstr "Đang thêm nhóm mới “%s” (GID %d) …\n"

#: ../adduser:431
#, perl-format
msgid "Adding new user `%s' (UID %d) with group `%s' ...\n"
msgstr "Đang thêm người dùng mới “%s” (UID %d) với nhóm “%s” …\n"

#: ../adduser:449 ../AdduserCommon.pm:162
#, perl-format
msgid "`%s' returned error code %d. Exiting.\n"
msgstr "“%s” trả lại mã lỗi %d nên thoát.\n"

#: ../adduser:451 ../AdduserCommon.pm:164
#, perl-format
msgid "`%s' exited from signal %d. Exiting.\n"
msgstr "“%s” bị chấm dứt do tín hiệu %d nên thoát.\n"

#: ../adduser:453
#, perl-format
msgid ""
"%s failed with return code 15, shadow not enabled, password aging cannot be "
"set. Continuing.\n"
msgstr ""
"%s bị lỗi với mã trả lại 15, bóng không phải được hiệu lực, không thể lập "
"thời gian sử dụng mật khẩu. Đang tiếp tục.\n"

#: ../adduser:478
#, perl-format
msgid "Adding user `%s' ...\n"
msgstr "Đang thêm người dùng “%s” …\n"

#: ../adduser:486
#, perl-format
msgid ""
"No UID/GID pair is available in the range %d-%d (FIRST_UID - LAST_UID).\n"
msgstr ""
"Không có cặp UID/GID nào sẵn sàng trong phạm vi %d-%d (FIRST_UID - "
"LAST_UID).\n"

#: ../adduser:498
#, perl-format
msgid "No UID is available in the range %d-%d (FIRST_UID - LAST_UID).\n"
msgstr ""
"Không có UID nào sẵn sàng trong phạm vi %d-%d (FIRST_UID - LAST_UID).\n"

#: ../adduser:514
#, perl-format
msgid "Adding new group `%s' (%d) ...\n"
msgstr "Đang thêm nhóm mới “%s” (%d) …\n"

#: ../adduser:521
#, perl-format
msgid "Adding new user `%s' (%d) with group `%s' ...\n"
msgstr "Đang thêm người dung mới “%s” (%d) với nhóm “%s” …\n"

#. hm, error, should we break now?
#: ../adduser:545
msgid "Permission denied\n"
msgstr "Không đủ quyền\n"

#: ../adduser:546
msgid "invalid combination of options\n"
msgstr "sai kết hợp các tùy chọn\n"

#: ../adduser:547
msgid "unexpected failure, nothing done\n"
msgstr "bị lỗi bất thường mà không làm gì\n"

#: ../adduser:548
msgid "unexpected failure, passwd file missing\n"
msgstr "bị lỗi bất thường, tập tin mật khẩu passwd còn thiếu\n"

#: ../adduser:549
msgid "passwd file busy, try again\n"
msgstr "Tập tin mật khẩu passwd đang bận, hãy thử lại\n"

#: ../adduser:550
msgid "invalid argument to option\n"
msgstr "sai lập đối số tới tùy chọn\n"

#. Translators: [y/N] has to be replaced by values defined in your
#. locale.  You can see by running "locale noexpr" which regular
#. expression will be checked to find positive answer.
#: ../adduser:555
msgid "Try again? [y/N] "
msgstr "Thử lại ? [c/K]"

#. Translators: [y/N] has to be replaced by values defined in your
#. locale.  You can see by running "locale yesexpr" which regular
#. expression will be checked to find positive answer.
#: ../adduser:581
msgid "Is the information correct? [Y/n] "
msgstr "Thông tin này có đúng chưa? [c/K]"

#: ../adduser:588
#, perl-format
msgid "Adding new user `%s' to extra groups ...\n"
msgstr "Đang thêm người dung mới “%s” vào các nhóm bổ sung …\n"

#: ../adduser:614
#, perl-format
msgid "Setting quota for user `%s' to values of user `%s' ...\n"
msgstr ""
"Đang lập hạn ngạch của người dùng “%s” thành giá trị của người dùng “%s” …\n"

#: ../adduser:651
#, perl-format
msgid "Not creating home directory `%s'.\n"
msgstr "Không tạo thư mục chính “%s”.\n"

#: ../adduser:654
#, perl-format
msgid "The home directory `%s' already exists.  Not copying from `%s'.\n"
msgstr "Thư mục chính “%s” đã có nên không sao chép từ “%s”.\n"

#: ../adduser:660
#, perl-format
msgid ""
"Warning: The home directory `%s' does not belong to the user you are "
"currently creating.\n"
msgstr "Cảnh báo: thư mục chính “%s” không thuộc về người dùng bạn đang tạo.\n"

#: ../adduser:665
#, perl-format
msgid "Creating home directory `%s' ...\n"
msgstr "Đang tạo thư mục chính “%s” …\n"

#: ../adduser:667
#, perl-format
msgid "Couldn't create home directory `%s': %s.\n"
msgstr "Không thể tạo thư mục chính “%s”: %s.\n"

#: ../adduser:675
#, perl-format
msgid "Copying files from `%s' ...\n"
msgstr "Đang sao chép các tập tin từ “%s” …\n"

#: ../adduser:677
#, perl-format
msgid "fork for `find' failed: %s\n"
msgstr "gặp lỗi khi phân nhánh tiến trình “find”: %s\n"

#: ../adduser:782
#, perl-format
msgid "The user `%s' already exists, and is not a system user.\n"
msgstr ""
"Người dùng “%s” đã có và không phải là một người dùng ở mức hệ thống.\n"

#: ../adduser:784
#, perl-format
msgid "The user `%s' already exists.\n"
msgstr "Người dùng “%s” đã có.\n"

#: ../adduser:787
#, perl-format
msgid "The UID %d is already in use.\n"
msgstr "UID %d đang được dùng.\n"

#: ../adduser:794
#, perl-format
msgid "The GID %d is already in use.\n"
msgstr "GID %d đang được dùng.\n"

#: ../adduser:801
#, perl-format
msgid "The GID %d does not exist.\n"
msgstr "GID %d không tồn tại.\n"

#: ../adduser:848
#, perl-format
msgid ""
"Cannot deal with %s.\n"
"It is not a dir, file, or symlink.\n"
msgstr ""
"Không thể xử lý %s.\n"
"Nó không phải là một thư mục, tập tin hoặc liên kết tượng trưng.\n"

#: ../adduser:868
#, perl-format
msgid ""
"%s: To avoid problems, the username should consist only of\n"
"letters, digits, underscores, periods, at signs and dashes, and not start "
"with\n"
"a dash (as defined by IEEE Std 1003.1-2001). For compatibility with Samba\n"
"machine accounts $ is also supported at the end of the username\n"
msgstr ""
"%s: Để tránh vấn đề thì tên người dùng nên chứa chỉ\n"
"chữ cái, chữ số, dấu gạch dưới, dấu chấm, dấu và (@), dấu gạch nối,\n"
"mà không cho phép bắt đầu với dấu gạch nối\n"
"(như xác định trong tiêu chuẩn quốc tế IEEE Std 1003.1-2001).\n"
"Để tương thích với tài khoản máy kiểu Samba\n"
"thì cũng hỗ trợ có dấu đô-la ($) kết thúc tên người dùng.\n"

#: ../adduser:876
msgid "Allowing use of questionable username.\n"
msgstr "Đang cho phép sử dụng một tên người dùng đáng ngờ.\n"

#: ../adduser:880
#, perl-format
msgid ""
"%s: Please enter a username matching the regular expression configured\n"
"via the NAME_REGEX configuration variable.  Use the `--force-badname'\n"
"option to relax this check or reconfigure NAME_REGEX.\n"
msgstr ""
"%s: Hãy gõ một tên người dùng tương ứng với biểu thức chính quy\n"
"được lập bằng biến cấu hình NAME_REGEX.\n"
"Dùng tùy chọn “--force-badname” để tránh sự kiểm tra này\n"
"hoặc để cấu hình lại NAME_REGEX.\n"

#: ../adduser:896
#, perl-format
msgid "Selecting UID from range %d to %d ...\n"
msgstr "Đang lựa chọn UID trong phạm vi %d đến %d …\n"

#: ../adduser:914
#, perl-format
msgid "Selecting GID from range %d to %d ...\n"
msgstr "Đang lựa chọn GID trong phạm vi %d đến %d …\n"

#: ../adduser:958
#, perl-format
msgid "Stopped: %s\n"
msgstr "Bị dừng: %s\n"

#: ../adduser:960
#, perl-format
msgid "Removing directory `%s' ...\n"
msgstr "Đang gỡ bỏ thư mục “%s” …\n"

#: ../adduser:964 ../deluser:358
#, perl-format
msgid "Removing user `%s' ...\n"
msgstr "Đang gỡ bỏ người dùng “%s” …\n"

#: ../adduser:968 ../deluser:403
#, perl-format
msgid "Removing group `%s' ...\n"
msgstr "Đang gỡ bỏ nhóm “%s” …\n"

#. Translators: the variable %s is INT, QUIT, or HUP.
#. Please do not insert a space character between SIG and %s.
#: ../adduser:979
#, perl-format
msgid "Caught a SIG%s.\n"
msgstr "Nhận được một tín hiệu SIG%s.\n"

#: ../adduser:984
#, perl-format
msgid ""
"adduser version %s\n"
"\n"
msgstr ""
"adduser phiên bản %s\n"
"\n"

#: ../adduser:985
msgid ""
"Adds a user or group to the system.\n"
"  \n"
"Copyright (C) 1997, 1998, 1999 Guy Maor <maor@debian.org>\n"
"Copyright (C) 1995 Ian Murdock <imurdock@gnu.ai.mit.edu>,\n"
"                   Ted Hajek <tedhajek@boombox.micro.umn.edu>\n"
"\n"
msgstr ""
"Thêm vào hệ thống một người dùng hay nhóm.\n"
"  \n"
"Tác quyền © năm 1997, 1998, 1999 của Guy Maor <maor@debian.org>\n"
"Tác quyền © năm 1995 của Ian Murdock <imurdock@gnu.ai.mit.edu>,\n"
"                   Ted Hajek <tedhajek@boombox.micro.umn.edu>\n"
"\n"

#: ../adduser:992 ../deluser:466
msgid ""
"This program is free software; you can redistribute it and/or modify\n"
"it under the terms of the GNU General Public License as published by\n"
"the Free Software Foundation; either version 2 of the License, or (at\n"
"your option) any later version.\n"
"\n"
"This program is distributed in the hope that it will be useful, but\n"
"WITHOUT ANY WARRANTY; without even the implied warranty of\n"
"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU\n"
"General Public License, /usr/share/common-licenses/GPL, for more details.\n"
msgstr ""
"Chương trình này là phần mềm tự do; bạn có thể phát hành lại nó\n"
"và/hoặc sửa đổi nó với điều kiện của Giấy Phép Công Cộng GNU\n"
"như được xuất bản bởi Tổ Chức Phần Mềm Tự Do; hoặc phiên bản 2\n"
"của Giấy Phép này, hoặc (tùy chọn) bất kỳ phiên bản sau nào.\n"
"\n"
"Chương trình này được phát hành vì mong muốn nó có ích,\n"
"nhưng KHÔNG CÓ BẢO HÀNH GÌ CẢ, THẬM CHÍ KHÔNG CÓ BẢO ĐẢM\n"
"ĐƯỢC NGỤ Ý KHẢ NĂNG BÁN HAY KHẢ NĂNG LÀM ĐƯỢC VIỆC DỨT KHOÁT.\n"
"Xem Giấy Phép Công Cộng GNU (/usr/share/common-licenses/GPL)\n"
"để biết thêm chi tiết.\n"

#: ../adduser:1006
msgid ""
"adduser [--home DIR] [--shell SHELL] [--no-create-home] [--uid ID]\n"
"[--firstuid ID] [--lastuid ID] [--gecos GECOS] [--ingroup GROUP | --gid ID]\n"
"[--disabled-password] [--disabled-login] [--add_extra_groups] USER\n"
"   Add a normal user\n"
"\n"
"adduser --system [--home DIR] [--shell SHELL] [--no-create-home] [--uid ID]\n"
"[--gecos GECOS] [--group | --ingroup GROUP | --gid ID] [--disabled-"
"password]\n"
"[--disabled-login] [--add_extra_groups] USER\n"
"   Add a system user\n"
"\n"
"adduser --group [--gid ID] GROUP\n"
"addgroup [--gid ID] GROUP\n"
"   Add a user group\n"
"\n"
"addgroup --system [--gid ID] GROUP\n"
"   Add a system group\n"
"\n"
"adduser USER GROUP\n"
"   Add an existing user to an existing group\n"
"\n"
"general options:\n"
"  --quiet | -q      don't give process information to stdout\n"
"  --force-badname   allow usernames which do not match the\n"
"                    NAME_REGEX configuration variable\n"
"  --help | -h       usage message\n"
"  --version | -v    version number and copyright\n"
"  --conf | -c FILE  use FILE as configuration file\n"
"\n"
msgstr ""
"adduser [--home THƯ_MỤC] [--shell HỆ_VỎ] [--no-create-home] [--uid MÃ_SỐ]\n"
"[--firstuid MÃ_SỐ] [--lastuid MÃ_SỐ] [--gecos GECOS] [--ingroup NHÓM | --gid "
"MÃ_SỐ]\n"
"[--disabled-password] [--disabled-login] [--add_extra_groups] NGƯỜI_DÙNG\n"
"   Thêm một người dùng thông thường\n"
"\n"
"adduser --system [--home THƯ_MỤC] [--shell HỆ_VỎ] [--no-create-home] [--uid "
"MÃ_SỐ]\n"
"[--gecos GECOS] [--group | --ingroup NHÓM | --gid MÃ_SỐ] [--disabled-"
"password]\n"
"[--disabled-login] [--add_extra_groups] NGƯỜI_DÙNG\n"
"   Thêm một người dùng ở mức hệ thống\n"
"\n"
"adduser --group [--gid MÃ_SỐ] NHÓM\n"
"addgroup [--gid MÃ_SỐ] NHÓM\n"
"   Thêm một nhóm ở mức người dùng\n"
"\n"
"addgroup --system [--gid MÃ_SỐ] NHÓM\n"
"   Thêm một nhóm ở mức hệ thống\n"
"\n"
"adduser NGƯỜI_DÙNG NHÓM\n"
"   Thêm vào một nhóm tồn tại một người dùng đã có\n"
"\n"
"Tùy chọn chung:\n"
"  --quiet | -q      đừng gửi cho đầu xuất tiêu chuẩn thông tin về diễn biến\n"
"  --force-badname   cho phép tên người dùng không tương ứng\n"
"                    với biến cấu hình NAME_REGEX\n"
"  --help | -h       trợ giúp này\n"
"  --version | -v    hiển thị phiên bản và bản quyền\n"
"  --conf | -c TẬP_TIN sử dụng tập tin này làm tập tin cấu hình\n"
"\n"

#. everyone can issue "--help" and "--version", but only root can go on
#: ../deluser:99
msgid "Only root may remove a user or group from the system.\n"
msgstr ""
"Chỉ siêu quản trị có quyền gỡ bỏ khỏi hệ thống một người dùng hay nhóm.\n"

#: ../deluser:120
msgid "No options allowed after names.\n"
msgstr "Không cho phép tùy chọn đẳng sau tên.\n"

#: ../deluser:128
msgid "Enter a group name to remove: "
msgstr "Nhập tên nhóm cần gỡ bỏ: "

#: ../deluser:130
msgid "Enter a user name to remove: "
msgstr "Nhập tên người dùng cần gỡ bỏ: "

#: ../deluser:170
msgid ""
"In order to use the --remove-home, --remove-all-files, and --backup "
"features,\n"
"you need to install the `perl' package. To accomplish that, run\n"
"apt-get install perl.\n"
msgstr ""
"Để sử dụng các tính năng “--remove-home”, “--remove-all-files”\n"
"và “--backup features” thì bạn cần phải cài đặt gói “perl”.\n"
"Để làm như thế, chạy câu lệnh:\n"
"apt-get install perl\n"

#: ../deluser:219
#, perl-format
msgid "The user `%s' is not a system user. Exiting.\n"
msgstr ""
"Người dùng “%s” không phải là một người dùng ở mức hệ thống nên thoát.\n"

#: ../deluser:223
#, perl-format
msgid "The user `%s' does not exist, but --system was given. Exiting.\n"
msgstr "Người dùng “%s” không tồn tại mà đưa ra “--system” nên thoát.\n"

#: ../deluser:234
msgid "WARNING: You are just about to delete the root account (uid 0)\n"
msgstr "CẢNH BÁO: bạn sắp xóa bỏ tài khoản siêu quản trị (UID 0)\n"

#: ../deluser:235
msgid ""
"Usually this is never required as it may render the whole system unusable\n"
msgstr ""
"Rất ít yêu cầu hành vi này vì nó có thể gây ra toàn bộ hệ thống là vô ích.\n"

#: ../deluser:236
msgid "If you really want this, call deluser with parameter --force\n"
msgstr ""
"Nếu bạn thực sự muốn làm hành vi này, gọi deluser với tham số “--force” (ép "
"buộc)\n"

#: ../deluser:237
msgid "Stopping now without having performed any action\n"
msgstr "Đang dừng lại ngay bây giờ mà chưa làm gì\n"

#: ../deluser:248
msgid "Looking for files to backup/remove ...\n"
msgstr "Đang tìm tập tin cần sao lưu hoặc gỡ bỏ …\n"

#: ../deluser:251
#, perl-format
msgid "fork for `mount' to parse mount points failed: %s\n"
msgstr "lỗi phân nhánh “mount” để phân tích điểm gắn kết: %s\n"

#: ../deluser:261
#, perl-format
msgid "pipe of command `mount' could not be closed: %s\n"
msgstr "không thể đóng ống dẫn của lệnh “mount”: %s\n"

#: ../deluser:270
#, perl-format
msgid "Not backing up/removing `%s', it is a mount point.\n"
msgstr "Không sao lưu hoặc gỡ bỏ “%s” vì nó là một điểm gắn kết.\n"

#: ../deluser:277
#, perl-format
msgid "Not backing up/removing `%s', it matches %s.\n"
msgstr "Không sao lưu hoặc gỡ bỏ “%s” vì nó tương ứng với %s.\n"

#: ../deluser:309
#, perl-format
msgid "Cannot handle special file %s\n"
msgstr "Không quản lý được tập tin đặc biệt %s\n"

#: ../deluser:317
#, perl-format
msgid "Backing up files to be removed to %s ...\n"
msgstr "Đang sao lưu vào %s các tập tin cần gỡ bỏ …\n"

#: ../deluser:343
msgid "Removing files ...\n"
msgstr "Đang gỡ bỏ các tập tin …\n"

#: ../deluser:355
msgid "Removing crontab ...\n"
msgstr "Đang gỡ bỏ bảng định kỳ crontab …\n"

#: ../deluser:361
#, perl-format
msgid "Warning: group `%s' has no more members.\n"
msgstr "Cảnh báo: nhóm “%s” không còn có bộ phận lại.\n"

#: ../deluser:383
#, perl-format
msgid "getgrnam `%s' failed. This shouldn't happen.\n"
msgstr "getgrnam “%s” bị lỗi. Trường hợp này không nên xảy ra.\n"

#: ../deluser:388
#, perl-format
msgid "The group `%s' is not a system group. Exiting.\n"
msgstr "Nhóm “%s” không phải là một nhóm ở mức hệ thống nên thoát.\n"

#: ../deluser:392
#, perl-format
msgid "The group `%s' is not empty!\n"
msgstr "Nhóm “%s” không trống!\n"

#: ../deluser:398
#, perl-format
msgid "`%s' still has `%s' as their primary group!\n"
msgstr "“%s” vẫn còn có “%s” là nhóm chính!\n"

#: ../deluser:422
msgid "You may not remove the user from their primary group.\n"
msgstr "Không cho phép gỡ bỏ người dùng khỏi nhóm chính của họ.\n"

#: ../deluser:436
#, perl-format
msgid "The user `%s' is not a member of group `%s'.\n"
msgstr "Người dùng “%s” không thuộc về nhóm “%s”.\n"

#: ../deluser:439
#, perl-format
msgid "Removing user `%s' from group `%s' ...\n"
msgstr "Đang gỡ bỏ người dùng %s khỏi nhóm “%s” …\n"

#: ../deluser:458
#, perl-format
msgid ""
"deluser version %s\n"
"\n"
msgstr ""
"deluser phiên bản %s\n"
"\n"

#: ../deluser:459
msgid "Removes users and groups from the system.\n"
msgstr "Gỡ bỏ khỏi hệ thống các người dùng và nhóm.\n"

#: ../deluser:461
msgid ""
"Copyright (C) 2000 Roland Bauerschmidt <roland@copyleft.de>\n"
"\n"
msgstr ""
"Tác quyền © năm 2000 của Roland Bauerschmidt <roland@copyleft.de>\n"
"\n"

#: ../deluser:463
msgid ""
"deluser is based on adduser by Guy Maor <maor@debian.org>, Ian Murdock\n"
"<imurdock@gnu.ai.mit.edu> and Ted Hajek <tedhajek@boombox.micro.umn.edu>\n"
"\n"
msgstr ""
"deluser dựa vào phần mềm adduser của Guy Maor <maor@debian.org>, Ian "
"Murdock\n"
"<imurdock@gnu.ai.mit.edu> và Ted Hajek <tedhajek@boombox.micro.umn.edu>\n"
"\n"

#: ../deluser:479
msgid ""
"deluser USER\n"
"  remove a normal user from the system\n"
"  example: deluser mike\n"
"\n"
"  --remove-home             remove the users home directory and mail spool\n"
"  --remove-all-files        remove all files owned by user\n"
"  --backup                  backup files before removing.\n"
"  --backup-to <DIR>         target directory for the backups.\n"
"                            Default is the current directory.\n"
"  --system                  only remove if system user\n"
"\n"
"delgroup GROUP\n"
"deluser --group GROUP\n"
"  remove a group from the system\n"
"  example: deluser --group students\n"
"\n"
"  --system                  only remove if system group\n"
"  --only-if-empty           only remove if no members left\n"
"\n"
"deluser USER GROUP\n"
"  remove the user from a group\n"
"  example: deluser mike students\n"
"\n"
"general options:\n"
"  --quiet | -q      don't give process information to stdout\n"
"  --help | -h       usage message\n"
"  --version | -v    version number and copyright\n"
"  --conf | -c FILE  use FILE as configuration file\n"
"\n"
msgstr ""
"deluser NGƯỜI_DÙNG\n"
"  gỡ bỏ khỏi hệ thống một người dùng thông thường\n"
"  ví dụ: deluser van_thanh\n"
"\n"
"  --remove-home            gỡ bỏ thư mục riêng và nơi chứa thư của người "
"dùng\n"
"  --remove-all-files       gỡ bỏ tất cả các tập tin được người dùng sở hữu\n"
"  --backup                 trước khi gỡ bỏ thì cũng sao lưu\n"
"  --backup-to THƯ_MỤC      thư mục vào đó cần sao lưu.\n"
"                           Mặc định là thư mục hiện tại.\n"
"  --system                 chỉ gỡ bỏ nếu là một người dùng ở mức hệ thống\n"
"\n"
"delgroup NHÓM\n"
"deluser --group NHÓM\n"
"  gỡ bỏ một nhóm nào đó khỏi hệ thống\n"
"  ví dụ : deluser --group hoc_sinh\n"
"\n"
"  --system                 chỉ gỡ bỏ nếu là một nhóm ở mức hệ thống\n"
"  --only-if-empty          chỉ gỡ bỏ nếu không còn thành viên nào\n"
"\n"
"deluser NGƯỜI_DÙNG NHÓM\n"
"  gỡ bỏ khỏi nhóm này người dùng đưa ra\n"
"  ví dụ : deluser van_thanh hoc_sinh\n"
"\n"
"Tùy chọn chung:\n"
"  --quiet | -q      đừng gửi cho đầu ra tiêu chuẩn thông tin về tiến trình\n"
"  --help | -h       hiển thị trợ giúp này\n"
"  --version | -v    hiển thị phiên bản và bản quyền\n"
"  --conf | -c TẬP_TIN  sử dụng tập tin này làm tập tin cấu hình\n"
"\n"

#: ../AdduserCommon.pm:64 ../AdduserCommon.pm:70
#, perl-format
msgid "%s: %s"
msgstr "%s: %s"

#: ../AdduserCommon.pm:82
#, perl-format
msgid "`%s' does not exist. Using defaults.\n"
msgstr "“%s” không tồn tại nên dùng giá trị mặc định.\n"

#: ../AdduserCommon.pm:92
#, perl-format
msgid "Couldn't parse `%s', line %d.\n"
msgstr "Không thể phân tích cú pháp của “%s”, dòng %d.\n"

#: ../AdduserCommon.pm:97
#, perl-format
msgid "Unknown variable `%s' at `%s', line %d.\n"
msgstr "Không rõ biến “%s” tại “%s”, dòng %d.\n"

#: ../AdduserCommon.pm:175
#, perl-format
msgid "Could not find program named `%s' in $PATH.\n"
msgstr "Không tìm thấy chương trình “%s” trên đường dẫn mặc định $PATH.\n"
